﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Labs.Controllers
{
    //Это указывает на то, что путь сюда будет начинаться с /home
    [Route("Home")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        //Название метода HelloWorld, поэтому и путь будет /home/HelloWorld
        public ActionResult HelloWorld()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}